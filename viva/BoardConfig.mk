TARGET_OTA_ASSERT_DEVICE := viva,U9200

# Target arch settings
TARGET_ARCH := arm
TARGET_NO_BOOTLOADER := true
TARGET_NO_RADIOIMAGE := true
TARGET_BOARD_PLATFORM := omap4
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_CPU_VARIANT := cortex-a9
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_ARCH_VARIANT_CPU := cortex-a9
TARGET_ARCH_VARIANT_FPU := neon
TARGET_BOOTLOADER_BOARD_NAME := viva

# Kernel
TARGET_RECOVERY_FSTAB = device/huawei/viva/recovery.fstab
BOARD_KERNEL_BASE := 0x80000000
BOARD_KERNEL_PAGESIZE := 2048
TARGET_KERNEL_CONFIG := front_recovery_defconfig
TARGET_KERNEL_SOURCE := kernel/huawei/omap4
# Linaro GCC 5.3.1 sources: http://releases.linaro.org/components/toolchain/binaries/5.3-2016.05/arm-linux-gnueabi/gcc-linaro-5.3.1-2016.05-i686_arm-linux-gnueabi.tar.xz
KERNEL_TOOLCHAIN := $(ANDROID_BUILD_TOP)/../../gcc-linaro-5.3.1-2016.05/bin/
KERNEL_TOOLCHAIN_PREFIX := arm-linux-gnueabi-
BOARD_KERNEL_CMDLINE := androidboot.hardware=viva

# fix this up by examining /proc/partitions on a running device. (value * 1024)
BOARD_BOOTIMAGE_PARTITION_SIZE := 8388608
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 8388608
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1342177280
# 6329204736 - 16384 <encryption footer>
BOARD_USERDATAIMAGE_PARTITION_SIZE := 6329188352
BOARD_CACHEIMAGE_PARTITION_SIZE := 268435456
BOARD_FLASH_BLOCK_SIZE := 4096

BOARD_CUSTOM_BOOTIMG_MK := device/huawei/viva/custombootimg.mk

# File Systems
TARGET_USERIMAGES_USE_F2FS := true
TARGET_USERIMAGES_USE_EXT4 := true
BOARD_HAS_LARGE_FILESYSTEM := true
BOARD_SUPPRESS_SECURE_ERASE := true

# Graphics
TW_SCREEN_BLANK_ON_BOOT := true
TW_MAX_BRIGHTNESS := 250
TW_BRIGHTNESS_PATH := /sys/class/backlight/lcd/brightness
RECOVERY_GRAPHICS_FORCE_USE_LINELENGTH := true
TW_THEME := portrait_mdpi

# USB Mounting
TARGET_USE_CUSTOM_LUN_FILE_PATH := /sys/class/android_usb/f_mass_storage/lun/file
TW_MTP_DEVICE := /dev/mtp_usb

# Hardware
BOARD_HAS_NO_SELECT_BUTTON := true
TW_CUSTOM_CPU_TEMP_PATH := /sys/devices/platform/omap/omap_temp_sensor.0/temp1_input
TW_CUSTOM_BATTERY_PATH := /sys/class/power_supply/Battery
# HW ID
TW_USE_MODEL_HARDWARE_ID_FOR_DEVICE_ID := false
TW_FORCE_CPUINFO_FOR_DEVICE_ID := false

# No Download mode
TW_HAS_DOWNLOAD_MODE := false

# Storages
BOARD_HAS_NO_MISC_PARTITION := true
TW_DEFAULT_EXTERNAL_STORAGE := true
TW_INTERNAL_STORAGE_PATH := "/sdcard"
TW_INTERNAL_STORAGE_MOUNT_POINT := "sdcard"
TW_EXTERNAL_STORAGE_PATH := "/extSdCard"
TW_EXTERNAL_STORAGE_MOUNT_POINT := "extSdCard"
TW_FLASH_FROM_STORAGE := true
TW_SDEXT_NO_EXT4 := true
RECOVERY_SDCARD_ON_DATA := true

# Crypto
TW_CRYPTO_REAL_BLKDEV := "/dev/block/mmcblk0p20"
TW_CRYPTO_MNT_POINT := "/data"
TW_CRYPTO_KEY_LOC := "footer"
TW_INCLUDE_L_CRYPTO := true

# No partitioning SD Card
BOARD_HAS_NO_REAL_SDCARD := true

# Exclude SU
TW_EXCLUDE_SUPERSU := true

# SubVersion
TW_DEVICE_VERSION := 0
